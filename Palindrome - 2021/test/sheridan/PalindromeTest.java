package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		String word = "bob";
		assertTrue("Unable to validate if the word is a palindrome", Palindrome.isPalindrome(word));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		String word = "wrong";
		assertFalse("Unable to validate if the word is a palindrome", Palindrome.isPalindrome(word));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		String word = "a toyota";
		assertTrue("Unable to validate if the word is a palindrome", Palindrome.isPalindrome(word));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		String word = "a red toyota";
		assertFalse("Unable to validate if the word is a palindrome", Palindrome.isPalindrome(word));
	}
	
}
